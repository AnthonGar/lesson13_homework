#pragma once

#include <string>
#include <iostream>
#include <WinSock2.h>
#include <Windows.h>

class Client
{
public:
	Client();
	Client(std::string, SOCKET);
	~Client();

	std::string GetName()
	{
		return _name;
	}

	SOCKET getSocket()
	{
		return _socket;
	}

private:

	//Members:
	std::string _name;
	SOCKET _socket;
};