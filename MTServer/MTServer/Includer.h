#pragma once

//This file will include all the files.
#include "Helper.h"
#include "Server.h"
#include "WSAInitializer.h"
#include "Client.h"

#include <iostream>
#include <string>
#include <WinSock2.h>
#include <Windows.h>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <exception>
#include <vector>
#include <queue>
