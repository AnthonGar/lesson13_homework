#pragma once

#include "Helper.h"
#include "Client.h"
#include <WinSock2.h>
#include <Windows.h>
#include <iostream> 
#include <queue> 
#include <stack>

using namespace std;

class Server
{
public:
	//C-tor and D-tor
	Server();
	~Server();

	//Others:
	void serve(int);
	void accept();
	void clientHandler(SOCKET clientSocket);

private:

	//code:
	string code101(string, Helper);
	void code204(SOCKET, Helper);
	void code207(SOCKET, Helper);
	void code208(SOCKET, Helper);

	//Getters:
	int getPosition(string name);
	std::string getCurrName();
	std::string getNextName();

	//Members:
	queue<Client> _connectedClients;
	queue<string> _requests;
	SOCKET _serverSocket;
};