#include <streambuf>
#include "Server.h"
#include "Client.h"
#include "Helper.h"
#include <exception>
#include <stack>
#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <fstream>

using namespace std;

#define CLIENT_LENGHT 2
#define FILE_LENGTH 5

#define CLIENT_LOG_IN 200
#define CLIENT_ACCEPTED "101"

#define CLIENT_UPDATE 204

#define CLIENT_FINISH 207

#define CLIENT_EXIT 208

//File:
#define FILE_PATH "data.txt"
int getFileSize();
string getFileContant();

//----------C-tor & D-tor:----------
Server::Server()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

//----------Getters:----------
int Server::getPosition(string name)
{
	queue<Client> temp = _connectedClients;

	for (int i = 0; i < temp.size(); i++)
	{
		if (name == temp.front().GetName())
			return i+1;
		else
			temp.pop();
	}

	//If clients name isnt in the queue.
	return 0;
}

std::string Server::getCurrName()
{
	return _connectedClients.front().GetName();
}

std::string Server::getNextName()
{
	try
	{
		if (_connectedClients.size() > 1)
		{
			queue<Client> temp = _connectedClients;
			temp.pop();

			Client tempClient = temp.front();
			return tempClient.GetName();
		}
		return "";
	}
	catch (...)
	{
		return "";
	}
}

void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;


	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");


	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "PORT: " << port << std::endl;

	cout << "waiting for clients to connect" << endl;
	while (true)
	{
		accept();
	}
}

void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client

	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	else
		std::cout << "Client accepted." << std::endl;

	//Creating new client thread.
	std::thread t(&Server::clientHandler, this, client_socket);
	t.detach();
}

void Server::clientHandler(SOCKET clientSocket)
{
	//Helps:
	Client newClient;
	string message = "";
	Helper help;

	bool exit = false;

	//Clients:
	int code, clientNameSize;
	string clientName = "";

	cout << "we are in the new thread" << endl;
	while (!exit)
	{
		try
		{
			//Getting the code:
			code = help.getMessageTypeCode(clientSocket);
			cout << "the code is: " << code << endl;
			switch (code)
			{
				//When a clients wants to login we need to send him code 101.
			case CLIENT_LOG_IN:
				//Adding new client:
				clientNameSize = help.getIntPartFromSocket(clientSocket, CLIENT_LENGHT);
				clientName = help.getStringPartFromSocket(clientSocket, clientNameSize);
				newClient = Client::Client(clientName, clientSocket);
				_connectedClients.push(newClient);

				cout << "new client added to the queue: " << clientName << "    socket: " << clientSocket << endl;

				message = code101(clientName, help);
				if (message != "0")
					help.sendData(clientSocket, message);
				else
				{
					throw std::exception("constructing code101 message failed");
				}
				break;

				//When a client updates the shared file.
			case CLIENT_UPDATE:
				code204(clientSocket, help);
				break;

				//When a client is finished updating the shared file.
			case CLIENT_FINISH:
				code207(clientSocket, help);
				break;

				//When a client disconects
			case CLIENT_EXIT:
				code208(clientSocket, help);
				break;

				//When an unknown code is recived.
			default:
				throw  std::exception("Invalid code was sent");
				break;
			}
		}
		catch (const std::exception& e)
		{
			cout << "unexpected error: " << e.what() << endl;
			closesocket(clientSocket);
		}
		// Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket);
	}

}

//----------Codes:----------
void Server::code208(SOCKET clientSocket, Helper help)
{
	//Removing the client:
	Client tempClient;
	queue<Client> temp = _connectedClients;
	queue<Client> newQueue;
	stack<Client> reverseStack;

	//Manking a new queue without the discconected client:
	for (int i = 0; i < temp.size(); i++)
	{
		tempClient = temp.front();
		if (tempClient.getSocket() != clientSocket)
			newQueue.push(tempClient);
		temp.pop();
		//We get a reversed queue and we need to reverse it back.
	}
	temp = _connectedClients;

	//reversing the queue:
	while (!temp.empty())
	{
		reverseStack.push(temp.front());
		temp.pop();
	}

	while (!reverseStack.empty())
	{
		newQueue.push(reverseStack.top());
		reverseStack.pop();
	}

	//New queue without the removed client:
	_connectedClients = newQueue;

	//Code 204 part:
	code204(clientSocket, help);
}

void Server::code207(SOCKET clientSocket, Helper help)
{
	//the same as code204 but also moving the client to the end of the queue.
	//Moving the client to the end:
	Client temp = _connectedClients.front();
	_connectedClients.pop();
	_connectedClients.push(temp);

	//Code 204 part:
	code204(clientSocket, help);
}

void Server::code204(SOCKET clientSocket, Helper help)
{

	//Getting the data:
	int fileSize = help.getIntPartFromSocket(clientSocket, FILE_LENGTH);
	string fileContent = help.getStringPartFromSocket(clientSocket, fileSize);

	//Writing to the file:
	ofstream file(FILE_PATH);
	if (file.is_open())
		file << fileContent;
	file.close();

	//Updating all the other users:
	string message = "";
	queue<Client> temp = _connectedClients;
	for (int i = 0; i < temp.size(); i++)
	{
		//Constructing message:
		message = code101(temp.front().GetName(),help);
		//fixing the position:
		message[message.size() - 1] = getPosition(temp.front().GetName());
		if (message != "0")
			//No errors: sending to a client.
			help.sendData(temp.front().getSocket(), message);
		else
			//There is an error
			throw std::exception("constructing code204 message failed");
	}
}

string Server::code101(std::string clientsName, Helper help)
{
	cout << "constracting code 101" << endl;
	string fileContent = "", currName = "", nextName = "", message = "", fileSizeStr = "", currNameSizeStr = "", nextNameSizeStr = "", codeToSend = "", position = "";

	//the 101 response:
	codeToSend = CLIENT_ACCEPTED;
	fileSizeStr = help.getPaddedNumber(getFileSize(), 5);
	cout << "got fileSizeStr: "<< fileSizeStr << endl;

	fileContent = getFileContant();
	cout << "got fileContant: " << fileContent << endl;

	currNameSizeStr = help.getPaddedNumber(getCurrName().size(), 2);
	cout << "got currNameSizeStr: "<< currNameSizeStr << endl;

	currName = getCurrName();
	cout << "got currName: " << currName << endl;

	nextNameSizeStr = help.getPaddedNumber(getNextName().size(), 2);
	cout << "got nextNameSizeStr: " << nextNameSizeStr << endl;

	nextName = getNextName();
	cout << "got nextName: " << nextName << endl;

	position = to_string(getPosition(clientsName));
	if (position == "0")
	{
		cout << "client isn't connected" << endl;
		return "0";
	}
	cout << "got position: " << position << endl;

	//Constructing the message to send to the client.
	message = "" + codeToSend + fileSizeStr + fileContent + currNameSizeStr + currName + nextNameSizeStr + nextName;
	cout << endl << endl << endl << message << endl;
	return message;
}

//----------Helpers:----------
int getFileSize()
{
	string fileName = FILE_PATH;
	ifstream file(fileName.c_str(), ifstream::in | ifstream::binary);

	if (!file.is_open())
	{
		return -1;
	}

	file.seekg(0, ios::end);
	int fileSize = file.tellg();
	file.close();

	return fileSize;
}

string getFileContant()
{
	string letter = "", buffer = "";

	ifstream file(FILE_PATH, ios::app);

	if (file.is_open())
	{
		while (getline(file, letter)) buffer += letter;

		file.close();
	}
	else 
		cout << "cant to open file";

	return buffer;
}

