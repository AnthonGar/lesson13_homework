#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>
#include "Helper.h"

#define PORT 8086

int main()
{
	try
	{
		WSAInitializer wsa;
		Server server;

		server.serve(PORT);
	}
	catch (std::exception& e)
	{
		cout << "unexpected error: " << e.what() << endl;
	}

	system("pause");
	return 0;
}
