#include "Client.h"

Client::Client()
{
	_socket = INVALID_SOCKET;
	_name = "";
}

Client::Client(std::string name, SOCKET socket)
{
	_socket = socket;
	_name = name;
}

Client::~Client()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_socket);
	}
	catch (...) {}
}